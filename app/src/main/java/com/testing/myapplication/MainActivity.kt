package com.testing.myapplication

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.testing.myapplication.ui.theme.MyApplicationTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                CardDesign()
            }
        }
    }
}

//@Preview(showBackground = true)
//Main Card Code
@Composable
fun CardDesign() {
    val isClicked = remember {
        mutableStateOf(false)
    }
    val context = LocalContext.current

    Surface(modifier = Modifier.fillMaxSize()) {
        Card(
            Modifier.padding(10.dp),
            elevation = 10.dp,
            shape = RoundedCornerShape(corner = CornerSize(15.dp)),
            backgroundColor = Color.White
        ) {

            Column(
                Modifier
                    .size(300.dp),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                CircularImage(Modifier.size(150.dp))
                Divider()
                UserInfo()
                Button(onClick = {
                    isClicked.value = !isClicked.value
                    Toast.makeText(context, "Button Clicked", Toast.LENGTH_SHORT).show()
                }, Modifier.fillMaxWidth(0.5f)) {
                    if (isClicked.value) {
                        Text(text = "Hide Portfolio")
                    } else {
                        Text(text = "View Portfolio")
                    }
                }
                if (isClicked.value) {
                    ListContent()

                } else {
                    Box {}
                }
            }
        }
    }
}

@Composable
//The information Card Code
fun UserInfo() {
    Column(
        Modifier
            .padding(15.dp)
            .fillMaxWidth(0.9f),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = "Manav Rajpal",
            color = Color.Black,
            fontSize = 18.sp,
            fontStyle = FontStyle.Normal
        )
        Text(
            text = "Mobile Application Developer",
            color = Color.Blue,
            fontSize = 15.sp,
            fontStyle = FontStyle.Normal
        )
        Text(
            text = "@manavrajpal",
            color = Color.Black,
            fontSize = 15.sp,
            fontStyle = FontStyle.Italic
        )
    }
}


@Composable
// Circular Image that is reuseable
private fun CircularImage(modifier: Modifier) {
    Surface(
        modifier
            .size(150.dp)
            .padding(10.dp),
        shape = CircleShape,
        border = BorderStroke(1.dp, color = Color.LightGray),
        elevation = 4.dp
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_launcher_background),
            contentDescription = "",
            modifier
                .size(135.dp),
            contentScale = ContentScale.Crop,
        )
    }
}

@Preview(showBackground = true)
//The box that is visible on button click
@Composable
fun ListContent() {
    Box(
        Modifier
            .fillMaxSize()
            .padding(15.dp),
    ) {
        Surface(
            Modifier.fillMaxSize(),
            shape = RoundedCornerShape(CornerSize(15.dp)),
            border = BorderStroke(2.dp, color = Color.Green),
            elevation = 10.dp,

            ) {
            Portfolio(listOf("Project 1", "Project 2", "Project 3", "Project 4"))
        }

    }
}

@Composable
//This is the Lazy Column that is known as RecyclerView in kotlin/ ListView Builder in flutter
fun Portfolio(data: List<String>) {
    LazyColumn {

        items(data) { item ->
            Card(
                Modifier
                    .padding(10.dp)
                    .fillMaxWidth(),
                elevation = 10.dp,
                shape = RoundedCornerShape(corner = CornerSize(15.dp)),
                backgroundColor = Color.White
            ) {
                Row(Modifier.padding(5.dp)) {
                    CircularImage(Modifier.size(100.dp))
                    Column(
                        Modifier
                            .padding(15.dp)
                            .align(Alignment.CenterVertically)) {
                        Text(
                            item,
                            fontSize = 15.sp,
                            fontWeight = FontWeight.Bold,
                            color = Color.Black
                        )
                        Text(
                            "First JetCompose Trail Project",
                            fontSize = 13.sp,
                            fontWeight = FontWeight.Thin,
                            color = Color.Black
                        )
                    }
                }
            }
        }
    }

}
